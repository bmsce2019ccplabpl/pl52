#include<stdio.h>

int main()
{
    int n,sum=0;
    printf("Enter the number of elements in the array\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter the array elements\n");
    for(int i=0;i<n;i++)
        scanf("%d",&a[i]);
    for(int j=0;j<n;j++)
        sum+=a[j];
    printf("The sum of all the elements of the array is %d\n",sum);
    return 0;
}
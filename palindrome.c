#include<stdio.h>
int input()
{
    int num;
    printf("enter the number\n");
    scanf("%d",&num);
    return num;
}
int compute(int num)
{
    int rev=0,rem;
    while(num>0)
    {
    rem=num%10;
    rev=rev*10+rem;
    num=num/10;
    }
    return rev;
}
void output(int rev ,int num)
{
    if(num==rev)
    printf("it is palindrome\n");
    else
    printf("it is not a palindrome");    
}
int main()
{
    int num,rev;
    num=input();
    rev=compute(num);
    output (rev ,num);
    return 0;
}
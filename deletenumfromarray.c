#include <stdio.h>

int main()
{
    int a[10],n,i,j,k,pos;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    printf("Enter the elements of the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("Enter the position from which the element is to be deleted\n");
    scanf("%d",&pos);
    for(j=pos;j<n;j++)
    {
        a[j]=a[j+1];
    }
    n=n-1;
    printf("The array after deletion of the element is\n");
    for(k=0;k<n;k++)
    {
        printf("%d \n",a[k]);
    }
    return 0;
}
del